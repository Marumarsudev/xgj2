using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchableObject : MonoBehaviour
{
    [SerializeField] private string ObjectName;

    private bool searchable = false;

    private string item = "";
    private bool collected = false;

    public void SetItem(string s)
    {
        item = s;
    }

    private void Update() 
    {
        if (searchable && Input.GetKeyDown(KeyCode.E))
        {
            if (!string.IsNullOrEmpty(item))
            {
                if (!collected)
                {
                    ScreenMessages.Message.Invoke($"Found {item}!");
                    collected = true;
                    Player.AddFoundItem.Invoke(item);
                    CollectedItemsHud.AddFoundItem.Invoke(item);
                }
                else
                {
                    ScreenMessages.Message.Invoke("Did not find anything!");
                }
            }
            else
            {
                ScreenMessages.Message.Invoke("Did not find anything!");
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.CompareTag("Player"))
        {
            if (!other.GetComponent<Player>())
            {
                return;
            }
            searchable = true;
            ActionPrompText.SetText.Invoke($"Press <color=#FFECBB><b><size=150> E </size></b></color> to search the {ObjectName}.");
        }
    }

    private void OnTriggerExit2D(Collider2D other) 
    {
        if (other.CompareTag("Player"))
        {
            searchable = false;
            ActionPrompText.SetText.Invoke("");
        }
    }
}
