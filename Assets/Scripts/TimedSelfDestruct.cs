using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class TimedSelfDestruct : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Invoke("DestroySelf", 2.5f);
    }

    private void DestroySelf()
    {
        var text = GetComponent<TextMeshProUGUI>();
        text.DOFade(0f, 0.7f).OnComplete(() => Destroy(gameObject));
        //Destroy(gameObject);
    }
}
