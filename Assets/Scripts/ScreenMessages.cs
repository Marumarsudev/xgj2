using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class ScreenMessages : MonoBehaviour
{
    public static readonly UnityEvent<string> Message = new UnityEvent<string>();
    [SerializeField] private GameObject prefab;

    // Start is called before the first frame update
    void Start()
    {
        Message.AddListener((s) => {
            var go = Instantiate(prefab, Vector3.zero, Quaternion.identity, transform);
            go.GetComponent<TextMeshProUGUI>().text = s;
        });
    }
}
