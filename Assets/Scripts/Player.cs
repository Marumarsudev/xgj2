using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
    public static readonly UnityEvent<Transform> MovePlayerTo = new UnityEvent<Transform>();
    public static readonly UnityEvent<string> AddFoundItem = new UnityEvent<string>();
    
    [SerializeField] private float moveSpeed = 1f;
    private Vector2 moveDir;
    private Rigidbody2D rb;

    private List<string> foundItems;
    public int GetFoundItemCount { get => foundItems.Count; }

    private AudioSource audioSource;

    [SerializeField] AudioClip[] footSteps = new AudioClip[0];
    [SerializeField] private float footStepInterval = 1f;
    private float footStepTimer = 0f;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        foundItems = new List<string>();
        MovePlayerTo.AddListener((t) => {
            gameObject.transform.position = t.position;
        });

        AddFoundItem.AddListener((i) => {
            foundItems.Add(i);
        });

        moveDir = new Vector2();
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (footStepTimer > 0f)
        {
            footStepTimer -= Time.deltaTime;
        }
        CheckInput();
    }

    void FixedUpdate() 
    {
        MoveCharacter();
    }

    private void CheckInput()
    {
        float moveX = Input.GetAxisRaw("Horizontal");
        float moveY = Input.GetAxisRaw("Vertical");

        moveDir = new Vector2(moveX, moveY).normalized;
    }

    private void MoveCharacter()
    {
        if (moveDir.magnitude > 0 && footStepTimer <= 0f)
        {
            footStepTimer = footStepInterval;
            audioSource.PlayOneShot(footSteps[Random.Range(0, footSteps.Length)]);
        }
        rb.velocity = moveDir * moveSpeed;
    }
}
