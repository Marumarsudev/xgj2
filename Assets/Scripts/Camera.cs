using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Camera : MonoBehaviour
{
    public static readonly UnityEvent<Transform> MoveCameraTo = new UnityEvent<Transform>();

    // Start is called before the first frame update
    void Start()
    {
        MoveCameraTo.AddListener((t) => {
            transform.position = new Vector3(t.position.x, t.position.y, -10);
        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
