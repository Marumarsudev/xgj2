using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class FadeInOnWake : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        var text = GetComponent<UnityEngine.UI.RawImage>();
        text.DOFade(0, 0.5f).SetDelay(1f);
    }
}
