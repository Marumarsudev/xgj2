using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class CollectedItemsHud : MonoBehaviour
{
    public static readonly UnityEvent<string> AddFoundItem = new UnityEvent<string>();

    [SerializeField] private GameObject textTemplate;

    private List<GameObject> texts;

    void Start()
    {
        texts = new List<GameObject>();
        AddFoundItem.AddListener((s) => {
            var newT = Instantiate(textTemplate, Vector3.zero, Quaternion.identity, transform);
            newT.SetActive(true);
            newT.GetComponent<TextMeshProUGUI>().text = s;
            texts.Add(newT);
        });
    }
}
