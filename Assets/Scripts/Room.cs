using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    public GameObject[] doorColliders = new GameObject[4];
    public GameObject[] SiblingRooms = new GameObject[4];
    public Door[] doors = new Door[4];
    public Transform[] doorSpots = new Transform[4];
    public bool generated = false;
    public bool isHalfRoom = false;
    public bool isSpecial = false;
    public List<SearchableObject> searchableObjects;
    public string searchedObjectName;

    private void Awake()
    {
        for (int i = 0; i < doors.Length; i++)
            doors[i].doorSide = i;

        if (searchableObjects.Count > 0)
        {
            searchableObjects[Random.Range(0, searchableObjects.Count)].SetItem(searchedObjectName);
        }
    }
}
