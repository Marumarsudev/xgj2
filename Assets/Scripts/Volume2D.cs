using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class Volume2D : MonoBehaviour
{
    public AnimationCurve soundCurve;
    public Transform listenerTransform;
    public AudioSource audioSource;
    public float minDist=1;
    public float maxDist=400;
 
    void Update()
    {
        float dist = Vector3.Distance(transform.position, listenerTransform.position);
        float normalized = 1 - (dist - minDist) / (maxDist - minDist);
        audioSource.volume = soundCurve.Evaluate(normalized);
    }
}