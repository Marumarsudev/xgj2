using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using DG.Tweening;

public class RoomManager : MonoBehaviour
{
    [SerializeField] private GameObject[] specialRooms = new GameObject[0];
    private int specialIndex;
    public static int specialRoomCount; // The number of special rooms = the number of "lost/searcheable" objects
    [SerializeField] private GameObject[] roomPrefabs = new GameObject[0];
    [SerializeField] private GameObject centerRoomPrefab;
    [SerializeField] private GameObject[] halfRoomsR = new GameObject[0];
    [SerializeField] private GameObject[] halfRoomsU = new GameObject[0];
    [SerializeField] private GameObject[] halfRoomsL = new GameObject[0];
    [SerializeField] private GameObject[] halfRoomsD = new GameObject[0];
 
    [SerializeField] private Transform roomParent;

    [SerializeField] private GameObject monster;

    public static bool finderMapped = false;

    private Room[,] rooms;
    private List<Room> roomList;
    public static List<Room> RoomList { get; private set; }
    private List<GameObject> halfRoomList;
    private List<GameObject> specialRoomList;
    private GameObject centerRoom;

    private int mapSizeX, mapSizeY;

    [SerializeField] private int maxRoomCount;
    [SerializeField] private int halfRoomCount;

    [SerializeField] private RawImage fadeImage;
    public static RawImage FadeImage;

    private int maxIterations = 10;
    private float move = 10.8f;
    private float offsetX, offsetY;

    private bool generating = false;

    private void Awake()
    {
        specialRoomCount = specialRooms.Length;
        FadeImage = fadeImage;
        mapSizeX = mapSizeY = maxRoomCount;
        roomList = new List<Room>();
        halfRoomList = new List<GameObject>();
        specialRoomList = new List<GameObject>();
        RoomList = roomList;
        offsetX = -(move * (mapSizeX / 2));
        offsetY = -(move * (mapSizeY / 2));

        ClearMap();
        StartCoroutine(GenerateMap());
    }

    private void ClearMap()
    {
        finderMapped = false;
        generating = true;
        specialIndex = 0;
        rooms = new Room[mapSizeX, mapSizeY];
        roomList.ForEach(room => {
            Destroy(room.gameObject);
        });
        roomList.Clear();
        halfRoomList.ForEach(room => {
            Destroy(room.gameObject);
        });
        halfRoomList.Clear();
        specialRoomList.Clear();

        centerRoom = Instantiate(centerRoomPrefab, new Vector3(offsetX + ((mapSizeX / 2) * move), offsetY + ((mapSizeY / 2) * move), 0f), Quaternion.identity, roomParent);
        rooms[(mapSizeX / 2), (mapSizeY / 2)] = centerRoom.GetComponent<Room>();
        rooms[(mapSizeX / 2), (mapSizeY / 2)].generated = true;
        roomList.Add(rooms[(mapSizeX / 2), (mapSizeY / 2)]);
    }

    private IEnumerator GenerateMap()
    {
        while (roomList.Count < maxRoomCount)
        {
            for (int x = 0; x < mapSizeX; x++)
            {
                if (roomList.Count >= maxRoomCount)
                {
                        break;
                }
                for (int y = 0; y < mapSizeY; y++)
                {
                    if (roomList.Count >= maxRoomCount)
                    {
                        break;
                    }
                    if (rooms[x,y] != null)
                    {
                        continue;
                    }
                    int hasAdjacent = 0;

                    if (x > 0)
                    {
                        if (rooms[x - 1, y] != null)
                        {
                            hasAdjacent++;
                        }
                    }
                    if (y > 0)
                    {
                        if (rooms[x, y - 1] != null)
                        {
                            hasAdjacent++;
                        }
                    }
                    if (x < mapSizeX - 1)
                    {
                        if (rooms[x + 1, y] != null)
                        {
                            hasAdjacent++;
                        }
                    }
                    if (y < mapSizeY - 1)
                    {
                        if (rooms[x, y + 1] != null)
                        {
                            hasAdjacent++;
                        }
                    }
                    if (x > 0 && y > 0 && hasAdjacent > 1)
                    {
                        if (rooms[x - 1, y - 1] != null)
                        {
                            hasAdjacent++;
                        }
                    }
                    if (y > 0 && x < mapSizeX - 1 && hasAdjacent > 1)
                    {
                        if (rooms[x + 1, y - 1] != null)
                        {
                            hasAdjacent++;
                        }
                    }
                    if (x < mapSizeX - 1 && y > 0 && hasAdjacent > 1)
                    {
                        if (rooms[x + 1, y - 1] != null)
                        {
                            hasAdjacent++;
                        }
                    }
                    if (y < mapSizeY - 1 && x > 0 && hasAdjacent > 1)
                    {
                        if (rooms[x - 1, y + 1] != null)
                        {
                            hasAdjacent++;
                        }
                    }

                    if (hasAdjacent > 3 || hasAdjacent == 0)
                    {
                        continue;
                    }

                    if (x == mapSizeX / 2 && y == mapSizeY / 2)
                    {
                        continue;
                    }
                    else if (Random.Range(0f, 1f) > 0.7f)
                    {
                        GameObject newRoom = Instantiate(roomPrefabs[Random.Range(0, roomPrefabs.Length)], new Vector3(offsetX + (x * move), offsetY + (y * move), 0f), Quaternion.identity, roomParent);
                        rooms[x,y] = newRoom.GetComponent<Room>();
                        rooms[x,y].generated = true;
                        roomList.Add(newRoom.GetComponent<Room>());
                    }
                }
            }
            yield return null;
        }
        StartCoroutine(ConvertToHalfRoom());
    }

    private IEnumerator ConvertToHalfRoom()
    {
        int iteration = 0;
        while (halfRoomList.Count < halfRoomCount && iteration < maxIterations)
        {
            iteration++;
            for (int x = 0; x < mapSizeX; x++)
            {
                if (halfRoomList.Count >= halfRoomCount)
                {
                        break;
                }
                for (int y = 0; y < mapSizeY; y++)
                {
                    if (halfRoomList.Count >= halfRoomCount)
                    {
                        break;
                    }
                    if (rooms[x,y] == null)
                    {
                        continue;
                    }
                    if (rooms[x,y].isHalfRoom)
                    {
                        continue;
                    }

                    int hasAdjacent = 0;
                    int side = -1;

                    if (x > 0)
                    {
                        if (rooms[x - 1, y] != null)
                        {
                            hasAdjacent++; side = 0;
                        }
                    }
                    if (y < mapSizeY - 1)
                    {
                        if (rooms[x, y + 1] != null)
                        {
                            hasAdjacent++; side = 1;
                        }
                    }
                    if (x < mapSizeX - 1)
                    {
                        if (rooms[x + 1, y] != null)
                        {
                            hasAdjacent++; side = 2;
                        }
                    }
                    if (y > 0)
                    {
                        if (rooms[x, y - 1] != null)
                        {
                            hasAdjacent++; side = 3;
                        }
                    }

                    if (hasAdjacent > 1 || hasAdjacent == 0 || side == -1)
                    {
                        continue;
                    }

                    if (x == mapSizeX / 2 && y == mapSizeY / 2)
                    {
                        continue;
                    }
                    else if (true)
                    {
                        GameObject newRoom = null;
                        
                        roomList.Remove(rooms[x,y]);
                        Destroy(rooms[x,y].gameObject);

                        switch (side)
                        {
                            case 0:
                                newRoom = Instantiate(halfRoomsR[Random.Range(0, halfRoomsR.Length)], new Vector3(offsetX + (x * move), offsetY + (y * move), 0f), Quaternion.identity, roomParent);
                            break;

                            case 1:
                                newRoom = Instantiate(halfRoomsU[Random.Range(0, halfRoomsU.Length)], new Vector3(offsetX + (x * move), offsetY + (y * move), 0f), Quaternion.identity, roomParent);
                            break;

                            case 2:
                                newRoom = Instantiate(halfRoomsL[Random.Range(0, halfRoomsL.Length)], new Vector3(offsetX + (x * move), offsetY + (y * move), 0f), Quaternion.identity, roomParent);
                            break;

                            case 3:
                                newRoom = Instantiate(halfRoomsD[Random.Range(0, halfRoomsD.Length)], new Vector3(offsetX + (x * move), offsetY + (y * move), 0f), Quaternion.identity, roomParent);
                            break;
                        }

                        rooms[x,y] = newRoom.GetComponentInChildren<Room>();
                        rooms[x,y].isHalfRoom = true;
                        //roomList.Add(rooms[x,y]);
                        halfRoomList.Add(newRoom);
                    }
                }
            }
            yield return null;
        }



        if (halfRoomList.Count != halfRoomCount)
        {
            ClearMap();
            StartCoroutine(GenerateMap());
        }
        else
        {
            StartCoroutine(ConvertToSpecialRoom());
        }
    }

    private IEnumerator ConvertToSpecialRoom()
    {
        int iteration = 0;
        while (specialIndex < specialRooms.Length && iteration < maxIterations)
        {
            iteration++;
            Debug.Log("index: " + specialIndex);
            Debug.Log("length: " + specialRooms.Length);
            for (int x = 0; x < mapSizeX; x++)
            {
                if (specialIndex >= specialRooms.Length)
                {
                    Debug.LogWarning("First break");
                    break;
                }
                for (int y = 0; y < mapSizeY; y++)
                {
                    if (specialIndex >= specialRooms.Length)
                    {
                        Debug.LogWarning("Second break");
                        break;
                    }
                    if (rooms[x,y] == null)
                    {
                        Debug.LogWarning("Room is null");
                        continue;
                    }
                    if (rooms[x,y].isHalfRoom)
                    {
                        Debug.LogWarning("Room is half");
                        continue;
                    }
                    if (rooms[x,y].isSpecial)
                    {
                        Debug.LogWarning("Room is special");
                        continue;
                    }
                    if (x == mapSizeX / 2 && y == mapSizeY / 2)
                    {
                        continue;
                    }

                    bool adjacentSpecial = false;

                    specialRoomList.ForEach(specialRoom => {
                        if (Vector2.Distance(specialRoom.transform.position, rooms[x,y].transform.position) < 20)
                        {
                            adjacentSpecial = true;
                        }
                    });

                    if (adjacentSpecial)
                    {
                        Debug.LogWarning("Adjacent special");
                        continue;
                    }

                    if (Vector2.Distance(rooms[mapSizeX/2, mapSizeY/2].transform.position, rooms[x,y].transform.position) < 20)
                    {
                        Debug.LogWarning("Room too close");
                        continue;
                    }

                    if (Random.Range(0f, 1f) >= 0.5)
                    {
                        roomList.Remove(rooms[x,y]);
                        Destroy(rooms[x,y].gameObject);
                        GameObject newRoom = Instantiate(specialRooms[specialIndex], new Vector3(offsetX + (x * move), offsetY + (y * move), 0f), Quaternion.identity, roomParent);
                        rooms[x,y] = newRoom.GetComponent<Room>();
                        rooms[x,y].generated = true;
                        rooms[x,y].isSpecial = true;
                        roomList.Add(newRoom.GetComponent<Room>());
                        specialRoomList.Add(newRoom);
                        specialIndex++;

                        if (adjacentSpecial)
                        {
                            Debug.LogWarning("Vittu tein silti saatana");
                        }
                    }
                }
            }
            yield return null;
        }

        if (specialIndex < specialRooms.Length)
        {
            ClearMap();
            StartCoroutine(GenerateMap());
        }
        else
        {
            StartCoroutine(SetEnabledDoors());
        }
    }

    private IEnumerator SetEnabledDoors()
    {
        for (int x = 0; x < mapSizeX; x++)
        {
            for (int y = 0; y < mapSizeY; y++)
            {
                if (rooms[x,y] == null)
                    continue;

                // if (rooms[x,y].isHalfRoom)
                //     continue;

                if (x > 0)
                {
                    rooms[x,y].doors[0].IsEnabled = rooms[x - 1, y] != null;
                    rooms[x,y].doors[0].connectedRoom = rooms[x - 1, y] != null ? rooms[x - 1, y] : null;
                    rooms[x,y].doorColliders[0].layer = rooms[x,y].doors[0].IsEnabled ? LayerMask.NameToLayer("Door") : LayerMask.NameToLayer("Wall");
                }
                if (y < mapSizeY - 1)
                {
                    rooms[x,y].doors[1].IsEnabled = rooms[x, y + 1] != null;
                    rooms[x,y].doors[1].connectedRoom = rooms[x, y + 1] != null ? rooms[x, y + 1] : null;
                    rooms[x,y].doorColliders[1].layer = rooms[x,y].doors[1].IsEnabled ? LayerMask.NameToLayer("Door") : LayerMask.NameToLayer("Wall");
                }
                if (x < mapSizeX - 1)
                {
                    rooms[x,y].doors[2].IsEnabled = rooms[x + 1, y] != null;
                    rooms[x,y].doors[2].connectedRoom = rooms[x + 1, y] != null ? rooms[x + 1, y] : null;
                    rooms[x,y].doorColliders[2].layer = rooms[x,y].doors[2].IsEnabled ? LayerMask.NameToLayer("Door") : LayerMask.NameToLayer("Wall");
                }
                if (y > 0)
                {
                    rooms[x,y].doors[3].IsEnabled = rooms[x, y - 1] != null;
                    rooms[x,y].doors[3].connectedRoom = rooms[x, y - 1] != null ? rooms[x, y - 1] : null;
                    rooms[x,y].doorColliders[3].layer = rooms[x,y].doors[3].IsEnabled ? LayerMask.NameToLayer("Door") : LayerMask.NameToLayer("Wall");
                }
            }
        }
        generating = false;
        yield return null;
        ScanAstar();
    }

    private IEnumerator SpawnMonster()
    {
        Room room = null;
        float distance = float.MinValue;
        roomList.ForEach(r => {
            float d = Vector2.Distance(r.transform.position, centerRoom.transform.position);
            if (d > distance)
            {
                room = r;
                distance = d;
            }
        });

        monster.transform.position = room.transform.position;
        monster.SetActive(true);

        yield return null;
    }

    private void ScanAstar()
    {
        Debug.LogWarning("SCANNING");
        AstarPath.active.Scan();
        finderMapped = true;
        StartCoroutine(SpawnMonster());
        fadeImage.DOFade(0f, 1f);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R) && !generating)
        {
            ClearMap();
            StartCoroutine(GenerateMap());
        }
    }
}
