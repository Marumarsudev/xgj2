using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class ActionPrompText : MonoBehaviour
{
    public static readonly UnityEvent<string> SetText = new UnityEvent<string>();

    private TextMeshProUGUI text;

    void Start()
    {
        text = GetComponent<TextMeshProUGUI>();
        text.text = "";
        SetText.AddListener((value) => text.text = value);
    }
}
