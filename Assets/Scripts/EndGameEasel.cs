using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGameEasel : MonoBehaviour
{
    private bool searchable;
    private bool playerHasAllItems = false;


    private void Update() 
    {
        if (searchable && Input.GetKeyDown(KeyCode.E))
        {
            if (!playerHasAllItems)
            {
                ScreenMessages.Message.Invoke("Yet to find all of the supplies!");
            }
            else
            {
                SceneManager.LoadScene(3, LoadSceneMode.Single);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.CompareTag("Player"))
        {
            if (!other.GetComponent<Player>())
            {
                return;
            }
            if (other.GetComponent<Player>().GetFoundItemCount >= RoomManager.specialRoomCount)
            {
                playerHasAllItems = true;
            }
            searchable = true;
            ActionPrompText.SetText.Invoke($"Press <color=#FFECBB><b><size=150> E </size></b></color> to paint.");
        }
    }

    private void OnTriggerExit2D(Collider2D other) 
    {
        if (other.CompareTag("Player"))
        {
            searchable = false;
            ActionPrompText.SetText.Invoke("");
        }
    }
}
