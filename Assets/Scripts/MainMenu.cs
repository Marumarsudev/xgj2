using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject introText;
    [SerializeField] private List<GameObject> hideOnStart;
    [SerializeField] private RawImage image;

    public void StartGame()
    {
        image.DOFade(1f, 1f).OnComplete(() => {
            hideOnStart.ForEach(go => go.SetActive(false));
            introText.SetActive(true);
            image.DOFade(0, 1f).SetDelay(0.5f).OnComplete(() => {
                image.DOFade(1, 1f).SetDelay(2f).OnComplete(() => SceneManager.LoadScene(1, LoadSceneMode.Single));
            });
        });
        //SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    public void QuitGame()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
}
