using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideInObject : MonoBehaviour
{
    [SerializeField] private string HideEnd;
    [SerializeField] private string ExitEnd;

    private bool searchable = false;
    private bool hidden = false;

    private GameObject player;

    private void Update() 
    {
        if ((searchable || hidden) && Input.GetKeyDown(KeyCode.E))
        {
            hidden = !hidden;
            if (!hidden)
            {
                player.SetActive(true);
                ActionPrompText.SetText.Invoke($"Press <color=#FFECBB><b><size=150> E </size></b></color> to hide {HideEnd}.");
            }
            else
            {
                Monster.SawPlayerHide.Invoke();
                player.SetActive(false);
                ActionPrompText.SetText.Invoke($"Press <color=#FFECBB><b><size=150> E </size></b></color> to {ExitEnd}.");
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.CompareTag("Player"))
        {
            if (!other.GetComponent<Player>())
            {
                return;
            }
            player = other.gameObject;
            searchable = true;
            ActionPrompText.SetText.Invoke($"Press <color=#FFECBB><b><size=150> E </size></b></color> to hide {HideEnd}.");
        }
    }

    private void OnTriggerExit2D(Collider2D other) 
    {
        if (other.CompareTag("Player"))
        {
            searchable = false;
            ActionPrompText.SetText.Invoke("");
        }
    }
}
