using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.Events;

public class Monster : MonoBehaviour
{
    public static UnityEvent SawPlayerHide = new UnityEvent();

    public AnimationCurve movementCurve;
    public Transform targetPos;
    public float moveSpeed;
    private float moveSpeedBoost = 0;

    [SerializeField] private Transform playerPos;
    [SerializeField] private AudioClip[] footsteps = new AudioClip[0];
    [SerializeField] private AudioClip killSound;

    private AudioSource audioSource;
    private Seeker seeker;
    private Path currentPath;
    private int currentWp = 0;
    private Rigidbody2D rb;
    private bool pathEnded = true;
    private float curveTime;
    private bool scanning = false;
    private bool sawHide = false;
    private bool killedPlayer = false;
    [SerializeField] private float seenTimer = 0f;

    // Start is called before the first frame update
    void Start()
    {
        SawPlayerHide.AddListener(() => {

            void DidNotSee()
            {
                Debug.LogWarning("Did not see player hide!");
                sawHide = false;
            }

            var hit = Physics2D.Raycast(transform.position, (playerPos.position - transform.position).normalized, 20f, LayerMask.GetMask(new string[] {"Door","Wall", "PlayerHitBox"}));
            if (hit)
            {
                if (hit.collider.CompareTag("Player"))
                {
                    Debug.LogWarning("Saw player hide!");
                    sawHide = true;
                    if (!scanning)
                    {
                        GetPath(playerPos.position);
                    }
                }
                else
                {
                    DidNotSee();
                }
            }
            else
            {
                DidNotSee();
            }
        });

        audioSource = GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody2D>();
        seeker = GetComponent<Seeker>();
    }

    private void GetPath(Vector3 to)
    {
        currentWp = 0;
        scanning = true;
        seeker.StartPath(transform.position, to, OnPathComplete);
    }

    // Update is called once per frame
    void Update()
    {
        if (seenTimer > 0f)
        {
            seenTimer -= Time.deltaTime;
        }
        else if (moveSpeedBoost > 0f)
        {
            moveSpeedBoost -= Time.deltaTime;
        }

        if (sawHide && Vector2.Distance(transform.position, playerPos.position) < 1f && !killedPlayer)
        {
            killedPlayer = true;
            audioSource.PlayOneShot(killSound);
            StartCoroutine(SwapScene());
            RoomManager.FadeImage.DOFade(1f, 0f);
        }

        if (Vector2.Distance(transform.position, playerPos.position) < 10f || seenTimer > 0f)
        {
            var hit = Physics2D.Raycast(transform.position, (playerPos.position - transform.position).normalized, 20f, LayerMask.GetMask(new string[] {"Door","Wall", "PlayerHitBox"}));
            if ((hit.collider.CompareTag("Player")))
            {
                moveSpeedBoost = Mathf.Clamp(moveSpeedBoost + Time.deltaTime, 0f, 2f);
                seenTimer = 10f;
            }

            if (!scanning && seenTimer > 0)
            {
                GetPath(playerPos.position);
            }
        }

        if (currentPath != null && !pathEnded)
        {
            float distToWp = Vector2.Distance(transform.position, currentPath.vectorPath[currentWp]);
            if (distToWp < 1f)
            {
                if (currentPath.vectorPath.Count > (currentWp + 1))
                {
                    currentWp++;
                }
                else
                {
                    pathEnded = true;
                }
            }
        }

        if (!pathEnded)
        {
            curveTime += Time.deltaTime;
            if (curveTime > 1f)
            {
                audioSource.PlayOneShot(footsteps[Random.Range(0, footsteps.Length)]);
            }
            curveTime = curveTime > 1 ? 0 : curveTime;

            Vector2 movedir = (currentPath.vectorPath[currentWp] - transform.position).normalized;

            //Debug.LogWarning(movedir * moveSpeed * Time.deltaTime * movementCurve.Evaluate(curveTime));
            rb.velocity = movedir * (moveSpeed + moveSpeedBoost) * movementCurve.Evaluate(curveTime);
        }
        else
        {
            if (RoomManager.finderMapped && !scanning)
            {
                Debug.LogWarning($"Fider: {RoomManager.finderMapped}");
                GetPath(RoomManager.RoomList[Random.Range(0, RoomManager.RoomList.Count)].transform.position);
            }
            rb.velocity = Vector2.zero;
        }

    }

    private void OnPathComplete(Path p)
    {
        currentPath = p;
        pathEnded = false;
        scanning = false;
        //Debug.Log(currentPath.vectorPath.Count);
    }

    IEnumerator SwapScene()
    {
        while (audioSource.isPlaying)
        {
            yield return null;
        }
        SceneManager.LoadScene(2, LoadSceneMode.Single);
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.CompareTag("Player") && !killedPlayer)
        {
            killedPlayer = true;
            audioSource.PlayOneShot(killSound);
            StartCoroutine(SwapScene());
            RoomManager.FadeImage.DOFade(1f, 0f);
        }
    }

}
