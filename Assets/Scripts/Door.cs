using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Door : MonoBehaviour
{
    [SerializeField] private GameObject ActualDoor;
    [SerializeField] private AudioClip openingSound;
    private AudioSource audioSource;

    public bool IsOpen { get => !ActualDoor.activeSelf; }
    public bool IsEnabled { get => gameObject.activeSelf; set => gameObject.SetActive(value); }

    public Room connectedRoom;

    public int doorSide;
    private int spot = -1;
    private static bool fading = false;

    private void Start() 
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.CompareTag("Player"))
        {
            switch (doorSide)
            {
                case 0:
                    spot = 2;
                break;

                case 1:
                    spot = 3;
                break;

                case 2:
                    spot = 0;
                break;

                case 3:
                    spot = 1;
                break;
            }
            ActionPrompText.SetText.Invoke("Press <color=#FFECBB><b><size=150> E </size></b></color> to open the door.");
        }
        else if (other.CompareTag("Monster"))
        {

        }
    }

    private void OnTriggerExit2D(Collider2D other) 
    {
        if (other.CompareTag("Player"))
        {
            spot = -1;
            ActionPrompText.SetText.Invoke("");
        }
    }

    private void Update() 
    {
        if (spot > -1 && Input.GetKeyDown(KeyCode.E) && !fading)
        {
            fading = true;
            audioSource.PlayOneShot(openingSound);
            RoomManager.FadeImage.DOFade(1f, 0.2f).OnComplete(() => {
                Player.MovePlayerTo.Invoke(connectedRoom.doorSpots[spot]);
                Camera.MoveCameraTo.Invoke(connectedRoom.transform);
                spot = -1;
                RoomManager.FadeImage.DOFade(0f, 0.2f)
                .SetDelay(0.1f)
                .OnComplete(() => fading = false);
            });
        }
    }

}
